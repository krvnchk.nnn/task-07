package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String SELECT_ALL_USERS = "SELECT * FROM users u ORDER BY u.login";
	private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams t ORDER BY t.name";
	private static final String INSERT_USER = "INSERT INTO users VALUES(DEFAULT, ?)";
	private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String INSERT_USERS_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
	private static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login = ?";
	private static final String GET_TEAM_BY_ID = "SELECT id, name FROM teams WHERE id = ?";
	private static final String GET_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name = ?";
	private static final String GET_USER_TEAMS = "SELECT team_id FROM users_teams WHERE user_id = ? ORDER BY team_id";
	private static final String DELETE_USERS_BY_ID = "DELETE FROM users WHERE id = ";
	private static final String DELETE_TEAM_BY_ID = "DELETE FROM teams WHERE id = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

	private static final String APP_PROPS_FILE = "app.properties";
	private static String FULL_URL;
	private static DBManager instance;

	static {
		try {
			FULL_URL = Files.readString(Path.of(APP_PROPS_FILE)).replace("connection.url=", "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private DBManager() {
	}

	public static synchronized DBManager getInstance() {
		if (DBManager.instance == null) {
			DBManager.instance = new DBManager();
		}
		return DBManager.instance;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(FULL_URL);
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(SELECT_ALL_USERS)) {
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setLogin(rs.getString("login"));
				users.add(u);
			}
		} catch (SQLException e) {
			throw new DBException("Exception", e);
		}
		return users;

	}

	public boolean insertUser(User user) throws DBException {
		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

			int k = 0;
			st.setString(++k, user.getLogin());

			int count = st.executeUpdate();
			if (count > 0){
				try(ResultSet rs = st.getGeneratedKeys()){
					if(rs.next()){
						user.setId(rs.getInt(1));
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		Statement st;
		String query;
		try {
			con = DriverManager.getConnection(FULL_URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			st = con.createStatement();
			for (User user : users) {
				query = DELETE_USERS_BY_ID + user.getId();
				st.executeUpdate(query);
			}

			con.commit();
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				assert con != null;
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("exception", e);
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		User u = new User();

		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(GET_USER_BY_LOGIN)) {

			int k = 0;
			st.setString(++k, login);

			ResultSet rs = st.executeQuery();
			rs.next();

			u.setId(rs.getInt("id"));
			u.setLogin(rs.getString("login"));

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}

		return u;
	}

	public Team getTeam(String name) throws DBException {
		Team t = new Team();

		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(GET_TEAM_BY_NAME)) {

			st.setString(1, name);

			ResultSet rs = st.executeQuery();
			rs.next();

			t.setId(rs.getInt("id"));
			t.setName(rs.getString("name"));

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}

		return t;
	}

	private Team getTeamById(int id) throws DBException {
		Team t = new Team();

		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(GET_TEAM_BY_ID)) {

			int k = 0;
			st.setInt(++k, id);

			ResultSet rs = st.executeQuery();
			rs.next();

			t.setId(rs.getInt("id"));
			t.setName(rs.getString("name"));

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}

		return t;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(FULL_URL);
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(SELECT_ALL_TEAMS)) {
			while (rs.next()) {
				Team t = new Team();
				t.setId(rs.getInt("id"));

				t.setName(rs.getString("name"));
				teams.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {

			int k = 0;
			st.setString(++k, team.getName());

			int count = st.executeUpdate();
			if (count > 0){
				try(ResultSet rs = st.getGeneratedKeys()){
					if(rs.next()){
						team.setId(rs.getInt(1));
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement st;

		try {
			con = DriverManager.getConnection(FULL_URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			for (Team team : teams) {
				st = con.prepareStatement(INSERT_USERS_TEAMS);

				int k = 0;
				st.setInt(++k, user.getId());
				st.setInt(++k, team.getId());

				st.executeUpdate();
			}

			con.commit();
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				assert con != null;
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("DBException", e);

		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new LinkedList<>();

		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(GET_USER_TEAMS)) {

			int k = 0;
			st.setInt(++k, user.getId());

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("team_id");
				teams.add(getTeamById(id));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(Connection con = DriverManager.getConnection(FULL_URL);
			PreparedStatement st = con.prepareStatement(DELETE_TEAM_BY_ID)) {

			int k= 0;
			st.setInt(++k, team.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);

		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {

		try (Connection con = DriverManager.getConnection(FULL_URL);
			 PreparedStatement st = con.prepareStatement(UPDATE_TEAM)) {

			int k = 0;
			st.setString(++k, team.getName());
			st.setInt(++k, team.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBException", e);
		}
		return true;
	}
}
